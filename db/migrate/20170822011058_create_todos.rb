class CreateTodos < ActiveRecord::Migration[5.1]
  def change
    create_table :todos do |t|
      t.integer :completed
      t.text :task

      t.timestamps
    end
  end
end
